# Technical Back End Developer Assingment
If you made it all the way here, congratulations! A new challenge has arrived though. 💪

Time for a technical assignment! Take as much time as you need to deliver your solution, while being reasonable. Please let us know if there are any unexpected delays or reasons why you can't take the test at this time. For any further questions, contact us via the Hiring Manager that has been in contact with you.


## Instructions
+ Clone this repo
+ Complete this exercise and submit either a zip of the solution or a link to a new repo
+ Please target the latest stable release of Laravel
+ Use the [OpenWeatherMap API](https://openweathermap.org/api) for weather data


## Requirements
We at TinkerList don't like any available calendar management systems, so we set-out to build our own! Currently all the development team is incredibly busy, so we decided to deploy our best and newest resource into action: You! 💪
This should be a very inicial MVP, meaning the solution should as extensible as possible for future developments and the model should take performance and re-usability into account. For this first MVP we require some specific endpoints that provide specific functionality:
+ Add new Event
  + An Event should include, but not be limited to, the following info: creator, event Date and time, Location (name), Invitees.
  + Send an email to any invited email addresses.
+ Get All the Events for a specific date interval
  + For each event, list what is the expected weather prediction for that day/time: general description, temperature, percipitation chance
  + These should be ordered by event date, and this endpoint should support pagination
+ Get details of a specific Event
  + This should include weather prediction for that day/time: general description, temperature, percipitation chance
+ Delete a specific event
+ Update a specific event.
  + We could be updating any specific part of the event, such as: Event Date and Time, Location, Invitees.
+ Get all locations where we have events coming on a specific date interval
  + These should be ordered by event date and include the current weather forecast. If two different events are happening on the same location, that location should be returned only once.

This API should support multiple users, so make sure you accept an authentication token on your endpoints so you know what token has access to what events.


## Bonus Round
__Was this too easy?__
  + Basic authentication and new account registering
  + Solution hosted on your favourite hosting systems with a working data storage (whatever you'd feel more comfortable with), and actually sending invitation emails.
  + Mention of what AWS services you'd use to mitigate some shortcomings and scalability issues in the future
  + Surprise us :)


## Review process
Please include a README file listing how to make your solution run locally, how to execute the tests, and a general description of your delivered solution and process. Also very important is listing the URLs to access each implemented endpoints and the structure of the expected request payload/parameters.

During the review process we'll be looking at:
- Git History
- Solution architecture and structure
- Structure of Data model
- Code style
- Proper use of comments whenever relevant
- Automated (unit and integration) tests
- Applied development patterns with Laravel
- Coverage with automatic tests


## What's next
After delivering your solution we will be reviewing it and provide you some feedback on it. This might be in the shape of a new meeting with our Development Team, or an e-mail!

If during the process you'd like to provide any feedback about it, feel free to do it at any point via your assigned HR Manager! We're happy to improve our process 🙂
